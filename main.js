window.onload = function () {
  
  function validate_output(reg, str, id, message) {
    if (!reg.test(str)) {
      document.getElementById(id).innerHTML = message;
    } else {
      document.getElementById(id).innerHTML = "";
    }
  }
  
  function validate() {
    var text = /^[a-zA-Zа-яА-Я]+$/,
        tel3 = /^((8|\+7)(\-| )?)?(\d){2,3}(\-| )?(\d){2,3}(\-| )?((\d){2}(\-| )?)?((\d){2}(\-| )?)?$/,
        age = /^(\d){1,3}$/,
        email = /^[a-zA-Zа-яА-Я0-9-_\.]+@[a-zA-Zа-яА-Я0-9]+\.[A-z]{2,4}$/,
        SurName = document.getElementById("SurName").value,
        UserName = document.getElementById("NameUser").value,
        PhoneNumber = document.getElementById("phone").value,
        UAge = document.getElementById("UserAge").value,
        Email = document.getElementById("UserEmail").value;

    validate_output(text, SurName, "SurNameSpan", "<br>Введите фамилию! Не менее 3 символов");
    validate_output(text, UserName, "UNameSpan", "<br>Введите имя! Не менее 3 символов");
    validate_output(tel3, PhoneNumber, "PhoneSpan", "<br>Введите телефон! Не менее 6 символов");
    validate_output(age, UAge, "UserAgeSpan", "<br>Введите возраст! Не менее 1 символов");
    validate_output(email, Email, "emailSpan", "<br>Введите email! Email содержит @");
  }

  document.getElementById("button").onclick = validate;
};